﻿using System;

namespace week_21
{
    class Program
    {
        static void Main(string[] args)
        {
           ///Start the program with Clear();
           Console.Clear();

          /*
          A)_____________________________________________________ 
          for (int i = 1; i <= 5; i=i+1)
            {
                Console.WriteLine(i);
            }


                Console.ReadLine();
           ______________________________________________________
         */
         /* 
         B)_________________________________________________________
         var counter=5;
         var i=0;
         while(i<counter)
         {
            var a=i+1;
            Console.WriteLine($"{a}");
            i++;
         }
         ______________________________________________________
         */
         var counter=20;
         

        for (int i = 0; i <= 20; i=i+2)
        {
          Console.WriteLine(i);
            
            
            
          
        }


           
           
           //End the program with blank line and instructions
           Console.ResetColor();
           Console.WriteLine();
           Console.WriteLine("Press <Enter> to quit the program");
           Console.ReadKey();







        }
    }
}
